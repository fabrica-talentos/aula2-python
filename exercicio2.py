class Conta(object):
    def __init__(self, numeroConta, nome):
        self.numeroConta = numeroConta
        self.nome = nome
        self.saldo = 0

    def alterar_nome(self, novoNome):
        self.nome = novoNome
        return f'Nome alterado: {self.nome}'

    def depositar(self, valor):
        self.saldo += valor
        return f'Valor depositado: R${valor}'

    def sacar(self, valor):
        if self.saldo >= valor:
            self.saldo -= valor
            return f'Valor sacado: R${valor}'
        else:
            return f'Não foi possível sacar. Saldo indisponível.'

conta = None
while True:
    if conta is not None:
        print(f'Bom dia {conta.nome}!')

    print('1. Criar uma nova conta')
    print('2. Depositar')
    print('3. Sacar')
    print('4. Mostrar informações da conta')
    print('5. Alterar nome da conta')
    print('6. Finalizar')
    choice = input('Digite uma opção: ')

    if choice == '1':
        print('\n––––– Criando nova conta –––––')
        conta = Conta(input('Número da conta: '), input('Nome: '))
        print('Conta criada com sucesso!')
        input('\nPressione Enter para continuar')
        print("\n" * 10)

    elif choice == '2':
        print('\n –––––Depositando –––––')
        print(conta.depositar(float(input('Valor do deposito: R$'))))
        input('\nPressione Enter para continuar')
        print("\n" * 10)

    elif choice == '3':
        print('\n ––––– Sacando –––––')
        print(conta.sacar(float(input('Valor do saque: R$'))))
        input('\nPressione Enter para continuar')
        print("\n" * 10)


    elif choice == '4':
        print('\n ––––– Informações da conta –––––')
        print(f'Nome: {conta.nome}')
        print(f'Número da conta: {conta.numeroConta}')
        print(f'Saldo atual: {conta.saldo}')
        input('\nPressione Enter para continuar')
        print("\n" * 10)

    elif choice == '5':
        print('\n ––––– Alterando nome –––––')
        print(f'Nome atual: {conta.nome}')
        print(conta.alterar_nome(input('Digite o novo nome: ')))
        input('\nPressione Enter para continuar')
        print("\n" * 10)

    elif choice == '6':
        print('\nFinalizando...')
        break

    else:
        print('\n Opção inválida. Tente novamente.')
        input('\nPressione Enter para continuar')
        print("\n" * 10)