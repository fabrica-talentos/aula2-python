class CalculoImposto(object):
    def __init__(self):
        self.__taxa = 0

    @property
    def taxa(self):
        return round(self.__taxa, 2)

    @taxa.setter
    def taxa(self, valorTaxa):
        if valorTaxa < 0:
            self.__taxa = 0
        elif valorTaxa > 100:
            self.__taxa = 100
        else:
            self.__taxa = valorTaxa
