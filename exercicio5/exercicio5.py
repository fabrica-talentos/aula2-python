from classes import Veiculo
from classes import Loja

veiculosList = []
loja = Loja(veiculosList)

while True:
    print('◊ ––––– Bem vindo ––––– ◊')
    print('1. Cadastrar veículo')
    print('2. Comprar veículo')
    print('3. Listar veículos')
    print('4. Sair do programa')
    choice = input('Digite uma opção: ')

    if choice == '1':
        marca = input('Digite o nome da marca: ')
        modelo = input('Digite o nome do modelo: ')
        preco = int(input('Digite o preço do veículo: '))
        veiculo = Veiculo(marca, modelo, preco)
        veiculosList.append(veiculo)

    elif choice == '2':
        loja.listarVeiculos()
        print('\n')
        numeroVeiculo = int(input('Digite o número do veículo que deseja comprar: '))
        loja.comprarVeiculo(numeroVeiculo)

    elif choice == '3':
        loja.listarVeiculos()

    elif choice == '4':
        print('\nSaindo do programa')
        break

    else:
        print('\nOpção inválida. Tente novamente')
