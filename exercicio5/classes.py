class Veiculo:
    def __init__(self, marca, modelo, preco):
        self.marca = marca
        self.modelo = modelo
        self.preco = preco

class Loja:
    def __init__(self, veiculos):
        self.veiculos = veiculos

    def comprarVeiculo(self, numeroVeiculo):
        if 0 <= numeroVeiculo < len(self.veiculos):
            veiculoComprado = self.veiculos.pop(numeroVeiculo)
            print('Veículo comprado com sucesso!')
            print(f'Marca: {veiculoComprado.marca}')
            print(f'Modelo: {veiculoComprado.modelo}')
            print(f'Preço: {veiculoComprado.preco}')
        else:
            print("\nNúmero de veículo inválido!")

    def listarVeiculos(self):
        for i, veiculo in enumerate(self.veiculos):
            print(i, veiculo.marca, veiculo.modelo, veiculo.preco)
