class Pessoa(object):
    def __init__(self, nome, sexo, idade):
        self.nome = nome
        self.sexo = sexo
        self.idade = idade

class Cidadao(Pessoa):
    def __init__(self, nome, sexo, idade, cpf):
        super().__init__(nome, sexo, idade)
        self.cpf = cpf

cidadao = Cidadao('Thomas', 'Masculino', 21, '123-456-789')
print(f'Nome: {cidadao.nome}')
print(f'Sexo: {cidadao.sexo}')
print(f'Idade: {cidadao.idade}')
print(f'CPF: {cidadao.cpf}')