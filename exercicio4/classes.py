class ControlarDesconto(object):
    def __init__(self):
        self.preco_produto = 0
        self.__porcentagem_desconto = 0

    @property
    def porcentagem_desconto(self):
        return round(self.__porcentagem_desconto, 0)

    @porcentagem_desconto.setter
    def porcentagem_desconto(self, porcentagem):
        if porcentagem < 0 or porcentagem > 35:
            self.__porcentagem_desconto = 1
        else:
            self.__porcentagem_desconto = porcentagem

    def valorFinal(self):
        self.preco_produto -=(self.preco_produto * self.__porcentagem_desconto) / 100
        return f'Valor final do produto após o desconto de {self.__porcentagem_desconto}%: R${self.preco_produto}'