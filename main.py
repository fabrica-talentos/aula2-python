"""
import time
class Aluno(object):
    def __init__(self, nome, idade):
        self.nome = nome
        self.idade = idade
        self.nota1 = 0
        self.nota2 = 0

    def addNotas(self, nota1, nota2):
        self.nota1 = nota1
        self.nota2 = nota2

    def mediaNotas(self):
        return float((self.nota1 + self.nota2)/2)

    def situacaoAluno(self):
        if self.mediaNotas() >= 60:
            return f'{self.nome} está Aprovado!!'
        elif 30 <= self.mediaNotas() < 60:
            return f'{self.nome} está de NP3.'
        else:
            return f'{self.nome } esté Reprovado...'

print(dir(Aluno))


aluno1 = Aluno('Thomas', 21)
print(f'{aluno1.nome} tem {aluno1.idade} anos')
aluno1.addNotas(70, 50)
print(f'Média das notas do {aluno1.nome}: {aluno1.mediaNotas()}')
print(aluno1.situacaoAluno())

print('–––––––––– ◊ ––––––––––')

aluno2 = Aluno('Pedro Vilas', 20)
print(f'{aluno2.nome} tem {aluno2.idade} anos')
aluno2.addNotas(53, 45)
print(f'Média das notas do {aluno2.nome}: {aluno2.mediaNotas()}')
print(aluno2.situacaoAluno())

print('–––––––––– ◊ ––––––––––')

aluno3 = Aluno('João Marcos', 22)
print(f'{aluno3.nome} tem {aluno3.idade} anos')
(aluno3.addNotas(23, 35))
print(f'Média das notas do {aluno3.nome}: {aluno3.mediaNotas()}')
print(aluno3.situacaoAluno())



class Animal(object):
    pass

class Ave(Animal):
    pass

class BeijaFlor(Ave):
    pass



class Pai(object):
    nome = 'Carlos'
    sobrenome = 'Oliveira'
    residencia = 'Santa Rita'
    olhos = 'Azuis'

class Filha(Pai):
    nome = 'Luciana'
    olhos = 'Castanhos'
class Neta(Filha):
    nome = 'Maria'


print(Pai.nome, Filha.nome, Neta.nome)
print(Pai.residencia, Filha.residencia, Neta.residencia)
print(Pai.olhos, Filha.olhos, Neta.olhos)


class Pessoa(object):
    nome = None
    idade = None


    def __init__(self, nome, idade):
        self.nome = nome
        self.idade = idade

    def envelhecer(self):
        self.idade += 1

class Atleta(Pessoa):
    peso = None
    aposentado = None

    def __init__(self, nome, idade, peso):
        super().__init__(nome, idade)
        self.peso = peso

    def aquecer(self):
        print('Atleta está aquecendo...')
        time.sleep(2)
        print('Atleta aquecido!')

    def aposentar(self):
        self.aposentado = True
        print(f'O atleta {self.nome} aposentou...')

class Corredor(Atleta):
    def correr(self):
        print(f'O corredor {self.nome} está correndo!! 🏃🏃')

class Nadador(Atleta):
    def nadar(self):
        print(f'O nadador {self.nome} está nadando!! 🏊🏊')

class Ciclista(Atleta):
    def pedalar(self):
        print(f'O ciclista {self.nome} está pedalando!! 🚴🚴')

corredor = Corredor('Carlos', 20, 50)
nadador = Nadador('Maria', 20, 50)
ciclista = Ciclista('Thiago', 20, 50)

corredor.aquecer()
corredor.correr()
print('\n')

nadador.aquecer()
nadador.nadar()
print('\n')

ciclista.aquecer()
ciclista.pedalar()
print('\n')

corredor.aposentar()



def decorador(func):
    def wrapper():
        print('Antes de executar')
        func()
        print('Depois de executar')
    return wrapper


@decorador
def media_da_turma():
    nota_1 = 10
    nota_2 = 5
    print(f'Média = {(nota_1 + nota_2) / 2}')


media_da_turma()
"""
from classes import CalculoImposto

calc = CalculoImposto()

calc.taxa = 55.25
print(calc.taxa)

calc.taxa = 105.60
print(calc.taxa)

calc.taxa = -50
print(calc.taxa)
