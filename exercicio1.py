import time
import colorama

class Homem(object):
    pass

class Mulher(object):
    pass

while True:
    pessoa = None
    print('Criação de objeto')
    print('1. Homem')
    print('2. Mulher')
    print('3. Sair do programa')
    choice = input('Digite uma opção: ')

    if choice == '1':
        print(colorama.Style.BRIGHT,"Opção escolhida: Homem")
        pessoa = Homem()
        time.sleep(1)
        print ("\n" * 10)

    elif choice == '2':
        print(colorama.Style.BRIGHT,"Opção escolhida: Mulher")
        pessoa = Mulher()
        time.sleep(1)
        print("\n" * 10)

    elif choice == '3':
        print(colorama.Style.BRIGHT, "Saindo do programa...")
        break

    else:
        print(colorama.Style.BRIGHT,"Opção inválida. Tente novamente.")
        time.sleep(1)
        print("\n" * 10)
